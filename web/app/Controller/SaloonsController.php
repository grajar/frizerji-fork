<?php
App::uses('AppController', 'Controller');
/**
 * Saloons Controller
 *
 * @property Saloon $Saloon
 */
class SaloonsController extends AppController 
{
	public $uses = array('Saloon', 'User', 'Shift', 'Appointment', 'Customer', 'ServiceArchive', 'OpeningTime');
	public $components = array('Session', 'Auth');

	public function calendar()
	{
		if (!isset($this->request->params['named']['firstDay']) || !isset($this->request->params['named']['numDays']))
		{
			throw new NotFoundException(__('Napačni podatki za koledar'));
		}
		$isPublic = true;
		if ($this->Auth->user()){
			$isPublic = false;
		}
		$saloon_id = null;
		if ($isPublic){
			if (!isset($this->request->params['named']['saloon']))
			{	
				throw new NotFoundException(__('Napačni podatki za koledar'));
			}
			else
			{
				$saloon_id = $this->request->params['named']['saloon'];	
			}
		}
		else
		{
			$saloon_id = $this->Auth->user('saloon_id');
		}
		$firstDay = $this->request->params['named']['firstDay'];
		$numDay = $this->request->params['named']['numDays'];
		$lastDay = $this->_addDays($firstDay, $numDay);
		
		$shifts = $this->_getShifts($firstDay, $lastDay, $saloon_id);

		$calendarData = $this->_orderShifts($shifts, $firstDay, $saloon_id, $isPublic);

		$employees = $this->Saloon->User->find('list', array(
			'conditions' => array('saloon_id' => $saloon_id),
			'fields' => array('id', 'name')    // TODO: v selectu se mora izpisat ime in priimek, ne samo ime
		));

		$dates = array();
		for ($i=0; $i < $numDay; $i++)
		{
			$dates[] = $this->_addDays($firstDay, $i);
		}

		$openingTimes = $this->OpeningTime->find('list', array(
			'conditions' => array('saloon_id' => $saloon_id),
			'recursive' => -1,
			'order' => 'day',
			'fields' => array('open', 'close', 'id')
		));

		$openedRows = $this->_openingTimesToRowNumbers($openingTimes, $firstDay, $numDay, 30);
		
		if ($this->request->is('ajax')){
			$this->render(json_encode($calendarData), 'ajax');	
		}
		else {
			$this->set('calendarData', $calendarData);	
			$this->set('dates', $dates);
			$this->set('employees', $employees);
			$this->set('openedRows', $openedRows);
		}
		$this->render(debug($openedRows),'default');
	}

	public function _orderShifts($shifts, $firstDay, $saloon_id, $isPublic)
	{
		//order by days
		$days = array();
		foreach ($shifts as $shift)
		{
			$day = $this->_dayDiff($firstDay, $shift['Shift']['start']);
			$days[$day]["Shifts"][] = $shift;
		}

		//order by columns
		$calendar = array();
		foreach ($days as $dayNum => $day)
		{
			$dayOfWeek = $this->_dayOfWeek($days[$dayNum]['Shifts'][0]['Shift']['start']);
			$column = 0;
			foreach ($day['Shifts'] as $shift)
			{
				$overlap = false;
				if (isset($calendar[$dayNum][$column]))
				{
					foreach ($calendar[$dayNum][$column] as $existingShift)
					{
						if ($this->_shiftsOverlap($shift, $existingShift)){
							$overlap = true;
						}
					}
					if (!$overlap)
					{
						$calendar[$dayNum][$column][] = $shift;
					}
					else
					{
						$column++;
						$calendar[$dayNum][$column][] = $shift;
					}	
				}
				else
				{
					$calendar[$dayNum][$column][] = $shift;
				}
			}

			// če je koledar javen združi in zamenja vse appointmente v intervale, ki imajo samo start in stop za podatek
			if ($isPublic)
			{
				foreach ($calendar[$dayNum] as $colKey => $columns)
				{
					foreach ($columns as $shiftKey => $calShift)
					{
						if (!empty($calShift['Appointment']))
						{
							$intervals = array();
							$start = null;
							$end = null;
							$nextInterval = true;
							$first = true;
							foreach ($calShift['Appointment'] as $appointment)
							{
								$boundries = $this->_getAppointmentBoundries($appointment);

								if ($boundries['start'] == $end){
									$end = $boundries['stop'];
								}
								else if ($first)
								{
									$first = false;
									$start = $boundries['start'];
									$end = $boundries['stop'];
								}
								else {
									$intervals[] = array($start, $end);
									$start = $boundries['start'];
									$end = $boundries['stop'];
								}
							}
							$intervals[] = array($start, $end);
							$calendar[$dayNum][$colKey][$shiftKey]['Appointment'] = $intervals;
						}
					}		
				}
			}
		}
		return $calendar;
	}

	// Vrne vse podatke za koledar (ampak neurejene)
	public function _getShifts($firstDay, $lastDay, $saloon_id)
	{
		$queryOptions = array(
			'conditions' => array(
				'Shift.start >=' => $firstDay,
				'Shift.stop <=' => $lastDay
			),
			'order' => array('Shift.user_id', 'Shift.start'),
			'fields' => array('Shift.start', 'Shift.stop', 'Shift.id'),
			'contain' => array(
				'User' => array(
					'conditions' => array(
						'User.saloon_id' => $saloon_id,
						'User.active' => '1'
					),
					'fields' => array('User.id', 'User.name', 'User.surname', 'User.color'),
				),
				'Appointment' => array(
					'fields' => array('Appointment.time', 'Appointment.id', 'Appointment.customer_name'),
					'order' => 'Appointment.time',
					'ServiceArchive' => array(
						'fields' => array('ServiceArchive.duration', 'ServiceArchive.name')
					),
					'Customer' => array(
						'fields' => array('Customer.name', 'Customer.surname')
					),
				),				
			)
		);

		$shifts = $this->Shift->find('all', $queryOptions);
		return $shifts;
	}

	public function addShift(){
		if ($this->request->is('post')) {
			$this->Shift->create();
			if ($this->Shift->save($this->request->data)) {
				//$this->Session->setFlash(__('The shift has been saved'));
				//$this->redirect(array('action' => 'index'));
			} else {
				//$this->Session->setFlash(__('The shift could not be saved. Please, try again.'));
			}
		}
	}
	public function _timeToRowNumber($time, $interval){
		$view = new View($this);
		$timeHelper = $view->loadHelper('Time');
		$minutes = $timeHelper->format('i', $time, true);
		$hours = $timeHelper->format('H', $time, true);
		return ($minutes + $hours*60)/$interval;
	}

	public function _openingTimesToRowNumbers($times, $firstDay, $numDay, $interval)
	{
		$view = new View($this);
		$timeHelper = $view->loadHelper('Time');
		$dayOfWeek = $timeHelper->format('N', $firstDay, true);

		$rowNums = array();
		$minOpen = 24*60/$interval;
		$maxClose = 0;
		for($i = 0; $i<$numDay; $i++)
		{
			$day = ($dayOfWeek+$i) % 7;
			if ($day == 0){
				$day = 7;
			}
			$time = $times[$day];
			$open = key($time);
			$close = $time[$open];
			$openRow = $this->_timeToRowNumber($open, $interval);
			$closeRow = $this->_timeToRowNumber($close, $interval);
			$rowNums[] = array('open' => $openRow, 'close' => $closeRow);

			if ($minOpen > $openRow)
			{
				$minOpen = $openRow;
			}
			if($maxClose < $closeRow)
			{
				$maxClose = $closeRow;
			}
		}
		$rowNums['max'] = array('open' => $minOpen, 'close' => $maxClose);
		return $rowNums;
	}

	public function _getAppointmentBoundries($appointment)
	{
		$service = $this->ServiceArchive->find('first',array(
			'conditions' => array(
				'ServiceArchive.id' => $appointment['service_archive_id']
			),
			'fields' => array('HOUR(duration) AS h' , 'MINUTE(duration) AS i', 'SECOND(duration) AS s'),
			'recursive' => -1
		));
		$duration = DateInterval::createFromDateString(
			$service[0]['h'].' hours + '.
			$service[0]['i'].' minutes + '.
			$service[0]['s'].' seconds');

		$appointmentDT = new DateTime($appointment['time']);
		$appointmentStart = $appointmentDT->format("Y-m-d H:i:s");

		$appointmentDT->add($duration);
		$appointmentEnd = $appointmentDT->format("Y-m-d H:i:s");

		return array('start'=>$appointmentStart, 'stop'=> $appointmentEnd);
	}

	// vrne številko dneva v tednu (0-6)
	public function _dayOfWeek($datetime){
		$view = new View($this);
		$timeHelper = $view->loadHelper('Time');
		return ($timeHelper->format('N', $datetime, true))-1;
	}

	// preveri če se izmeni prekrivata
	public function _shiftsOverlap($shift1, $shift2){
		if ($shift1['Shift']['stop'] <= $shift2['Shift']['start'] || $shift1['Shift']['start'] >= $shift2['Shift']['stop']){
			return false;
		}
		else{
			return true;
		}
	}

	//vrne datum zadnjega dneva, ki je še zahtevan (prvi dan + število dni)
	public function _addDays($firstDay, $numDay){
		$lastDay = new DateTime($firstDay);
		$lastDay->add(new DateInterval('P'.$numDay.'D')); 
		$lastDay = $lastDay->format('Y-m-d');
		return $lastDay;
	}

	// extracta samo date del datetime-a
	public function _extractDate($datetime)
	{
		$date = new DateTime($datetime);
		return $date->format('Y-m-d');
	}

	//vrne pozitivno razliko dni med dvema datumoma
	public function _dayDiff($date1, $date2)
	{
		$datetime1 = new DateTime($this->_extractDate($date1));
		$datetime2 = new DateTime($this->_extractDate($date2));
		$difference = $datetime1->diff($datetime2, true);
		return $difference->format('%a');
	}

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Saloon->recursive = 0;
		$this->set('saloons', $this->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		$this->Saloon->id = $id;
		if (!$this->Saloon->exists()) {
			throw new NotFoundException(__('Invalid saloon'));
		}
		$this->set('saloon', $this->Saloon->read(null, $id));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Saloon->create();
			if ($this->Saloon->save($this->request->data)) {
				$this->Session->setFlash(__('The saloon has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The saloon could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		$this->Saloon->id = $id;
		if (!$this->Saloon->exists()) {
			throw new NotFoundException(__('Invalid saloon'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Saloon->save($this->request->data)) {
				$this->Session->setFlash(__('The saloon has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The saloon could not be saved. Please, try again.'));
			}
		} else {
			$this->request->data = $this->Saloon->read(null, $id);
		}
	}

/**
 * delete method
 *
 * @throws MethodNotAllowedException
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->Saloon->id = $id;
		if (!$this->Saloon->exists()) {
			throw new NotFoundException(__('Invalid saloon'));
		}
		if ($this->Saloon->delete()) {
			$this->Session->setFlash(__('Saloon deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Saloon was not deleted'));
		$this->redirect(array('action' => 'index'));
	}

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->Saloon->recursive = 0;
		$this->set('saloons', $this->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		$this->Saloon->id = $id;
		if (!$this->Saloon->exists()) {
			throw new NotFoundException(__('Invalid saloon'));
		}
		$this->set('saloon', $this->Saloon->read(null, $id));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->Saloon->create();
			if ($this->Saloon->save($this->request->data)) {
				$this->Session->setFlash(__('The saloon has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The saloon could not be saved. Please, try again.'));
			}
		}
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		$this->Saloon->id = $id;
		if (!$this->Saloon->exists()) {
			throw new NotFoundException(__('Invalid saloon'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Saloon->save($this->request->data)) {
				$this->Session->setFlash(__('The saloon has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The saloon could not be saved. Please, try again.'));
			}
		} else {
			$this->request->data = $this->Saloon->read(null, $id);
		}
	}

/**
 * admin_delete method
 *
 * @throws MethodNotAllowedException
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->Saloon->id = $id;
		if (!$this->Saloon->exists()) {
			throw new NotFoundException(__('Invalid saloon'));
		}
		if ($this->Saloon->delete()) {
			$this->Session->setFlash(__('Saloon deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Saloon was not deleted'));
		$this->redirect(array('action' => 'index'));
	}

	public function validate_form()
    { 
        return $this->ajax_validate_form();
    }
}
