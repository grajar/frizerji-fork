<?php
App::uses('AppModel', 'Model');
/**
 * Saloon Model
 *
 * @property Employee $Employee
 * @property Service $Service
 */
class Saloon extends AppModel {

/**
 * Display field
 *
 * @var string
 */

	public $displayField = 'name';
	
	public $actsAs = array('containable');
/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'name' => array(
			'minlength' => array(
				'rule' => array('minlength', 3),
				'message' => 'Ime vsebuje premalo znakov'
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),

		),
		'address' => array(
			'minlength' => array(
				'rule' => array('minlength', 4),
				'message' => 'Naslov vsebuje premalo znakov'
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'city' => array(
			'minlength' => array(
				'rule' => array('minlength', 2),
				'message' => 'Mesto vsebuje premalo znakov'
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		)
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'User' => array(
			'className' => 'User',
			'foreignKey' => 'saloon_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'Service' => array(
			'className' => 'Service',
			'foreignKey' => 'saloon_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'OpeningTime' => array(
			'className' => 'OpeningTime',
			'foreignKey' => 'saloon_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
	);
}
