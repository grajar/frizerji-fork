<?php
App::uses('AppModel', 'Model');
/**
 * Shift Model
 *
 * @property User $User
 * @property Appointment $Appointment
 */
class Shift extends AppModel
{
	public $displayField = 'id';
	public $actsAs = array('containable');
	public $validate = array(
		'start' => array(
			'datetime' => array(
				'rule' => array('datetime'),
				'message' => 'Napačna oblika zapisa',
				'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'stop' => array(
			'datetime' => array(
				'rule' => array('datetime'),
				'message' => 'Napačna oblika zapisa',
				'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
			'isGreaterThanStart' => array(
				'rule' => array('isGreaterThanStart'),
				'message' => 'Konec izmene mora biti po začetku izmene'
			),
			'employeeIsFree' => array(
				'rule' => array('employeeIsFree'),
				'message' => 'Izmena se prekriva z drugo izmeno te osebe'
			),
		),
	);
	
	public function employeeIsFree($check){
		$id = $this->data[$this->alias]['id'];
		$start = $this->data[$this->alias]['start'];
		$stop = $this->data[$this->alias]['stop'];
		$existingShifts = $this->find('all', array('conditions' => array('user_id' => $this->data[$this->alias]['user_id']), 'recursive' => -1));
		$valid = true;
		foreach ($existingShifts as $existingShift) {
			if ($id != $existingShift['Shift']['id']){
				if ($stop > $existingShift['Shift']['start'] && $stop < $existingShift['Shift']['stop']){
					$valid = false;
				}
				if ($start > $existingShift['Shift']['start'] && $start < $existingShift['Shift']['stop']){
					$valid = false;
				}
				if ($start <= $existingShift['Shift']['start'] && $stop >= $existingShift['Shift']['stop']){
					$valid = false;
				}
			}
		}
		return $valid;
		
	}

	public function isGreaterThanStart($check) {
		$start = new DateTime($this->data[$this->alias]['start']);
		$stop = new DateTime($this->data[$this->alias]['stop']);

		if ($start < $stop)	{
			return true;
		}
		else{
			return false;
		}
	}

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'User' => array(
			'className' => 'User',
			'foreignKey' => 'user_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'Appointment' => array(
			'className' => 'Appointment',
			'foreignKey' => 'shift_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

}
