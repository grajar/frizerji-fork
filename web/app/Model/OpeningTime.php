<?php
App::uses('AppModel', 'Model');
/**
 * OpeningTime Model
 *
 * @property Saloon $Saloon
 */
class OpeningTime extends AppModel {

/**
 * Validation rules
 *
 * @var array
 */
	
	public $actsAs = array('containable');
	public $validate = array(
		'open' => array(
			'time' => array(
				'rule' => array('time'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'close' => array(
			'time' => array(
				'rule' => array('time'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'day' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				'message' => 'Navedeni dan ni dan v tednu',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			)
		),
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Saloon' => array(
			'className' => 'Saloon',
			'foreignKey' => 'saloon_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
