<div class="openingTimes view">
<h2><?php  echo __('Opening Time'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($openingTime['OpeningTime']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Saloon'); ?></dt>
		<dd>
			<?php echo $this->Html->link($openingTime['Saloon']['name'], array('controller' => 'saloons', 'action' => 'view', $openingTime['Saloon']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Open'); ?></dt>
		<dd>
			<?php echo h($openingTime['OpeningTime']['open']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Close'); ?></dt>
		<dd>
			<?php echo h($openingTime['OpeningTime']['close']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Day'); ?></dt>
		<dd>
			<?php echo h($openingTime['OpeningTime']['day']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Opening Time'), array('action' => 'edit', $openingTime['OpeningTime']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Opening Time'), array('action' => 'delete', $openingTime['OpeningTime']['id']), null, __('Are you sure you want to delete # %s?', $openingTime['OpeningTime']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Opening Times'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Opening Time'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Saloons'), array('controller' => 'saloons', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Saloon'), array('controller' => 'saloons', 'action' => 'add')); ?> </li>
	</ul>
</div>
