<!DOCTYPE html>
<html>
<head>
	<?php echo $this->Html->charset(); ?>
	<title>
		<?php echo $title_for_layout; ?>
	</title>
	<?php
		echo $this->Html->meta('icon');
		echo $this->Html->css('cake.generic');
		echo $this->Html->css('bootstrap.min');
		echo $this->Html->script('jquery-1.9.1.min');
		echo $scripts_for_layout;
	?>
</head>
	<body>
	<div><?php echo $this->html->link(__('Zaposleni'), array('controller' => 'saloons', 'action' => 'employee')) ?></a></div>
	<div id="content_div">
		<?php echo $this->fetch('content'); ?>
	</div>
	<?php echo $this->Js->writeBuffer(array('cache' => true)); ?>
</body>

