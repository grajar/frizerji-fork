<div class="appointments form">
<?php echo $this->Form->create('Appointment'); ?>
	<fieldset>
		<legend><?php echo __('Admin Add Appointment'); ?></legend>
	<?php
		echo $this->Form->input('shift_id');
		echo $this->Form->input('service_archive_id');
		echo $this->Form->input('customer_id');
		echo $this->Form->input('time');
		echo $this->Form->input('confirmed');
		echo $this->Form->input('comment');
		echo $this->Form->input('customer_name');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Appointments'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Shifts'), array('controller' => 'shifts', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Shift'), array('controller' => 'shifts', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Service Archives'), array('controller' => 'service_archives', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Service Archive'), array('controller' => 'service_archives', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Customers'), array('controller' => 'customers', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Customer'), array('controller' => 'customers', 'action' => 'add')); ?> </li>
	</ul>
</div>
