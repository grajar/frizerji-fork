<div class="serviceArchives view">
<h2><?php  echo __('Service Archive'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($serviceArchive['ServiceArchive']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Name'); ?></dt>
		<dd>
			<?php echo h($serviceArchive['ServiceArchive']['name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Duration'); ?></dt>
		<dd>
			<?php echo h($serviceArchive['ServiceArchive']['duration']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Price'); ?></dt>
		<dd>
			<?php echo h($serviceArchive['ServiceArchive']['price']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Service Archive'), array('action' => 'edit', $serviceArchive['ServiceArchive']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Service Archive'), array('action' => 'delete', $serviceArchive['ServiceArchive']['id']), null, __('Are you sure you want to delete # %s?', $serviceArchive['ServiceArchive']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Service Archives'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Service Archive'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Appointments'), array('controller' => 'appointments', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Appointment'), array('controller' => 'appointments', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php echo __('Related Appointments'); ?></h3>
	<?php if (!empty($serviceArchive['Appointment'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Customer Id'); ?></th>
		<th><?php echo __('User Id'); ?></th>
		<th><?php echo __('Service Archive Id'); ?></th>
		<th><?php echo __('Time'); ?></th>
		<th><?php echo __('Confirmed'); ?></th>
		<th><?php echo __('Active'); ?></th>
		<th><?php echo __('Comment'); ?></th>
		<th><?php echo __('Customer Name'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Modified'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($serviceArchive['Appointment'] as $appointment): ?>
		<tr>
			<td><?php echo $appointment['id']; ?></td>
			<td><?php echo $appointment['customer_id']; ?></td>
			<td><?php echo $appointment['user_id']; ?></td>
			<td><?php echo $appointment['service_archive_id']; ?></td>
			<td><?php echo $appointment['time']; ?></td>
			<td><?php echo $appointment['confirmed']; ?></td>
			<td><?php echo $appointment['active']; ?></td>
			<td><?php echo $appointment['comment']; ?></td>
			<td><?php echo $appointment['customer_name']; ?></td>
			<td><?php echo $appointment['created']; ?></td>
			<td><?php echo $appointment['modified']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'appointments', 'action' => 'view', $appointment['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'appointments', 'action' => 'edit', $appointment['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'appointments', 'action' => 'delete', $appointment['id']), null, __('Are you sure you want to delete # %s?', $appointment['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Appointment'), array('controller' => 'appointments', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
