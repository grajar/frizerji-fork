<link rel="stylesheet" href="/css/smoothness/jquery-ui-1.10.0.custom.min.css" />
   <style>
  #feedback { font-size: 1.4em; }
  .selectable .ui-selecting { background: #FECA40; }
  .selectable .ui-selected { background: #F39814; color: white; }
  .selectable { list-style-type: none; margin: 0; padding: 0; width: 60%; }
  .selectable li { margin: 3px; padding: 0.4em; font-size: 1.4em; height: 18px; }
  </style>

<div class="row">
	<?php 
	$this->Html->script('calendar', array('block' =>'scripts'));
	for($i=0; $i<count($dates); $i++)
	{
		echo '<div class="span2">';
		echo '<ol class="selectable" day='.$dates[$i].'>';
			for($j = 0; $j <5; $j++)
			{
				echo '<li class="ui-widget-content">Item '.$j.'</li>';
			}
		echo '</ol>';
		echo '</div>';
	}
	?>
</div>