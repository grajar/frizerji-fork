<div id="employee_calendar">
	<?php		// iz $shifts sestavi in izriše koledar
				
	echo $this->Js->link('previous',array('controller' => 'saloons', 'action' => 'ajaxRefreshCalendar', $currentEmployee['Employee']['id'], $weekOffset-1),
								array('update'=>'#all'));			
	echo $this->Js->link('next',array('controller' => 'saloons', 'action' => 'ajaxRefreshCalendar', $currentEmployee['Employee']['id'], $weekOffset+1),
								array('update'=>'#all'));			?>		
</div>
<p><?php echo 'Week offset: '.$weekOffset ?></p>
<p><?php echo 'User: '.$currentEmployee['Employee']['name'] ?></p>

<div id="add_shift">
	<?php echo $this->Form->create('Shift'); ?>
	<fieldset>
		<legend><?php echo __('Nova izmena'); ?></legend>
	<?php
		echo $this->Form->input('employee_id', array('options' => array($currentEmployee['Employee']['id'] => $currentEmployee['Employee']['name'].' '.$currentEmployee['Employee']['surname'])));
		echo $this->Form->input('start', array('type' => 'datetime', 'interval' => 30, 'separator' => '', /*'class'=> */ )); 
		echo $this->Form->input('stop', array('type' => 'time', 'interval' => 30, 'separator' => '', /*'class'=> */ )); 
	?>
	</fieldset>
	<?php echo $this->Js->submit(__('Dodaj'), array('url' => array('action' => 'save_shift'), 'update' => '#all')); ?>	
	<?php echo $this->Form->end(null);?>	
</div>
<?php echo $this->Js->writeBuffer(array('cache' => true)); ?>	