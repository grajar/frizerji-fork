<div class="employee_list_div" id="employee_list">
	<?php 													// iz $employees izriše seznam zaposlenih
		foreach ($employees as $employee) {
			?>
			<div style="background-color:#BBBBBB">
			<?php
				echo $this->Js->link($employee['Employee']['name'].' '.$employee['Employee']['surname'],
					array('controller' => 'saloons', 'action' => 'ajaxRefreshCalendar', $employee['Employee']['id']),
					array('update'=>'#all'));
			?>
			</div>
			<?php 
		}
	?>
</div>
<div id="all">
	<?php 
	echo $this->element('employeeCalendar', array("currentEmployee" => $currentEmployee, "$weekOffset"));
	?>
</div>
