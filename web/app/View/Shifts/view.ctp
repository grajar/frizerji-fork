<div class="shifts view">
<h2><?php  echo __('Shift'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($shift['Shift']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('User'); ?></dt>
		<dd>
			<?php echo $this->Html->link($shift['User']['name'], array('controller' => 'users', 'action' => 'view', $shift['User']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Start'); ?></dt>
		<dd>
			<?php echo h($shift['Shift']['start']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Stop'); ?></dt>
		<dd>
			<?php echo h($shift['Shift']['stop']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Shift'), array('action' => 'edit', $shift['Shift']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Shift'), array('action' => 'delete', $shift['Shift']['id']), null, __('Are you sure you want to delete # %s?', $shift['Shift']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Shifts'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Shift'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?> </li>
	</ul>
</div>
